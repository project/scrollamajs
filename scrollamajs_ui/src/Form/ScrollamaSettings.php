<?php

namespace Drupal\scrollamajs_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Scrollama settings.
 */
class ScrollamaSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scrollama_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get current settings.
    $config = $this->config('scrollamajs.settings');

    // Let module handle load Scrollama.js library.
    $form['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Scrollama library'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the Scrollama library for your site. To prevent loading twice, leave this option disabled if you're including the assets manually or through another module or theme."),
    ];

    // Load method library from CDN or Locally.
    $form['version'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Use version'),
      '#options'       => [
        'v2' => $this->t('Version 2.2.3'),
        'v3' => $this->t('Version 3.2.0'),
      ],
      '#default_value' => $config->get('version'),
      '#description'   => $this->t('Select the latest stable version of the Scrollama library you want to use.'),
    ];

    // Load method library from CDN or Locally.
    $form['method'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Attach method'),
      '#options'       => [
        'local' => $this->t('Local'),
        'cdn'   => $this->t('CDN'),
      ],
      '#default_value' => $config->get('method'),
      '#description'   => $this->t('These settings control how the Scrollama library is loaded. You can choose to load from the CDN (External source) or from the local (Already included in Scrollama module).'),
    ];

    // Production or minimized version.
    $form['minimized'] = [
      '#type'  => 'details',
      '#title' => $this->t('Development or Production version'),
      '#open'  => TRUE,
    ];
    $form['minimized']['minimized_options'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Choose minimized or non-minimized library.'),
      '#options'       => [
        0 => $this->t('Use non-minimized library (Development)'),
        1 => $this->t('Use minimized library (Production)'),
      ],
      '#default_value' => $config->get('minimized.options'),
      '#description'   => $this->t('These settings work with both local library and CDN methods.'),
    ];

    // Load Scrollama.js library Per-path.
    $form['url'] = [
      '#type'  => 'details',
      '#title' => $this->t('Load on specific URLs'),
      '#open'  => FALSE,
    ];
    $form['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load Scrollama.js on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _scrollamajs_ui_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated Scrollama.js settings.
    $this->config('scrollamajs.settings')
      ->set('load', $values['load'])
      ->set('version', $values['version'])
      ->set('method', $values['method'])
      ->set('minimized.options', $values['minimized_options'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _scrollamajs_ui_string_to_array($values['url_pages']))
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'scrollamajs.settings',
    ];
  }

}
