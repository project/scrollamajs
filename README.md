INTRODUCTION
------------

This module integrates the 'Scrollama.js' library
  - https://github.com/russellsamora/scrollama

Scrollama is a modern & lightweight JavaScript library for scrollytelling
using IntersectionObserver in favor of scroll events.

Scrollytelling can be complicated to implement & difficult to make performant.
The goal of this library is to provide a simple interface for creating
scroll-driven interactive. Scrollama is focused on performance by using
IntersectionObserver to handle element position detection.


FEATURES
--------

  - Optimized performance

  - Lightweight (6KB gzipped)

  - Flexibility and extensibility

  - Mobile compatibility

  - Event management

  - Support for responsive web design

  - Object-oriented programming and object chaining

  - Readable, centralized code, and intuitive development

  - Support for both x and y direction scrolling (even both on one page)

  - Support for scrolling inside div containers (even multiple on one page)

  - Extensive debugging and logging capabilities

  - Detailed documentation

  - Many application examples


REQUIREMENTS
------------

No requirement (library already included).


INSTALLATION
------------

Quick and easy to use that load Scrollama library and UI pack on your pages:

1. Download 'Scrollama JS' module - https://www.drupal.org/project/scrollamajs

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/scrollamajs or /modules/scrollamajs

3. Now, enable 'Scrollama' module.

If you want to have more control over how to load, change the version,
use or not use the UI pack on pages (to limit it on some pages),
you can also activate the "Scrollama UI" submodule too.


USAGE
-----

It’s very simple to use a library, Add your script in theme/module js file.

<!--you don't need the "data-step" attr,
but can be useful for storing instructions for JS -->

<div class="step" data-step="a"></div>
<div class="step" data-step="b"></div>
<div class="step" data-step="c"></div>

// instantiate the scrollama
const scroller = scrollama();

// setup the instance, pass callback functions
scroller
  .setup({
    step: ".step",
  })
  .onStepEnter((response) => {
    // { element, index, direction }
  })
  .onStepExit((response) => {
    // { element, index, direction }
  });

For more usage check official document:

  - https://github.com/russellsamora/scrollama#examples


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://russellsamora.github.io/scrollama/basic
